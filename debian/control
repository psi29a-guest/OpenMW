Source: openmw
Section: contrib/games
Priority: optional
Maintainer: Bret Curtis <psi29a@gmail.com>
Build-Depends: debhelper (>= 9~), cmake (>= 3) | cmake3, libbullet-dev (>= 2.86),
 libboost-filesystem-dev, libboost-program-options-dev, libboost-thread-dev,
 libopenal-dev, libtinyxml-dev, libavcodec-dev, libavformat-dev,
 libavutil-dev, libswscale-dev, libswresample-dev, libsdl2-dev,
 libmygui-dev (>= 3.2.1), libunshield-dev, libopenscenegraph-3.4-dev,
 libqt5opengl5-desktop-dev, freeglut3-dev, libgl1-mesa-dev | libgl-dev,
Standards-Version: 4.1.3
Homepage: http://openmw.org
Vcs-Git: https://anonscm.debian.org/git/pkg-games/openmw.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-games/openmw.git

Package: openmw
Architecture: any
Recommends: openmw-launcher
Depends: ${shlibs:Depends}, ${misc:Depends}, openmw-data (= ${source:Version})
Description: Reimplementation of The Elder Scrolls III: Morrowind
 OpenMW is a reimplementation of the Bethesda Game Studios game
 The Elder Scrolls III: Morrowind.
 .
 The Morrowind "Data Files" from the original game are required to play.

Package: openmw-cs
Architecture: any
Recommends: openmw
Depends: ${shlibs:Depends}, ${misc:Depends}, openmw-data (= ${source:Version})
Description: Replacement of The Elder Scrolls Construction Set
 OpenCS is a replacement of the Bethesda Game Studios Elder
 Scrolls Construction Set which gives full control of the game's
 content and the ability to create new content.
 .
 While initialy to be used with Morrowind assets, it can be used to
 create TC and other content not depending on Morrowind.

Package: openmw-data
Architecture: all
Recommends: openmw
Depends: ${misc:Depends}, fonts-ebgaramond-extra, fonts-dejavu-core
Description: Resources for the OpenMW engine
 All the shaders, models, mygui xml files and extra assets necessary
 for running OpenMW.
 .
 Without this, OpenMW will complain about running Morrowind.

Package: openmw-launcher
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, openmw (>= ${source:Version})
Description: Launcher for OpenMW using the Qt-Gui-Toolkit
 Additional launcher for handling installation of Morrowind and its
 expansions, mods and configuration details of OpenMW.
 .
 While not necessary, it automates the setup of OpenMW to play
 Morrowind.

#Package: esmtool
#Section: utils
#Architecture: any
#Depends: ${shlibs:Depends}, ${misc:Depends}
#Description: Command-Line tool which can extract ESM-Files
# Command-Line tool which can extract ESM-Files.
